package com.program;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Doors {
    static int powerOfartifact;
    static int powerOfMonster;
    static int[] doors = new int[10];
    static int powerOfHero = 25;
    static Menu menu = new Menu();

    public static void Doors() throws IOException {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            doors[i] = i + 1;
            System.out.print(doors[i] + " ");
        }
        System.out.println();
    }

    public static void OpenDoor() throws IOException {
        System.out.println("Press the number of a door that you want to open:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int number = Integer.parseInt(reader.readLine());
        if (number % 3 == 0) {
            System.out.println("In case you open this door, the monster will take your power");
            printPowerHeromonster();
            Doors();
        } else {
            System.out.println("In case you open this door, you will get a power");
            printPowerArtifact();
            Doors();

        }
    }

    public static void printPowerArtifact() throws IOException {
        powerOfartifact = ((int) (Math.random() * 10) + 70);
        System.out.println(" This artifact will give you a power :" + powerOfartifact);
        powerOfHero += powerOfartifact;
        System.out.println(" The current hero`s power is:" + powerOfHero);
        System.out.println();
        OpenDoor();
    }

    public static void printPowerHeromonster() throws IOException {
        powerOfMonster = ((int) (Math.random() * 5) + 95);
        if (powerOfMonster <= powerOfHero) {
            powerOfHero -= powerOfMonster;
            System.out.println(" The current hero`s power is: " + powerOfHero);
            System.out.println(" The current monsters` power is: " + powerOfMonster);
            System.out.println(" Congratulations! You`re the winner! ");
            menu.choose();
        } else {
            powerOfHero = 0;
            System.out.println(" The current hero`s power is: " + powerOfHero);
            System.out.println(" The current monsters` power is: " + powerOfMonster);
            System.out.println(" Game over! ");
            powerOfHero = 25;
            System.out.println();
            menu.choose();
        }
    }
}
