package com.program;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class LogicalTasksForArrays {
    public static int[] CreateArray() throws IOException {
        System.out.println("Please, enter the size of your array: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int size = Integer.parseInt(reader.readLine());
        int[] array = new int[size];
        for (int count = 0; count < size; count++) {
            int number = (int) (Math.random() * (10) + 1);
            array[count] = number;
            System.out.print(number + " ");
        }
        System.out.println();
        return array;
    }

    public static void commonArray() throws IOException {
        int[] array1 = CreateArray();
        int[] array2 = CreateArray();
        int size3 = array1.length > array2.length ? array2.length : array1.length;
        int[] array3 = new int[size3];
        int count = 0;

        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    array3[count] = array1[i];
                    array2[j] = -1;
                    count++;
                }
            }
        }
        int[] res = new int[count];
        for (int i = 0; i < count; i++) {
            res[i] = array3[i];
        }
        System.out.println();
        Set<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < res.length; i++) {
            set.add(res[i]);
        }
        Iterator<Integer> it = set.iterator();
        while (it.hasNext()) {
            array3 = Arrays.copyOf(array3, array3.length + 1);
            array3[count] = it.next();
            System.out.print(array3[count] + " ");
        }
        System.out.println();

    }

    public static void AddUniqueValues() throws IOException {
        int[] array = CreateArray();
        int count = 0;
        int[] array3 = new int[0];

        Set<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < array.length; i++) {
            set.add(array[i]);
        }
        Iterator<Integer> it = set.iterator();
        while (it.hasNext()) {
            array3 = Arrays.copyOf(array3, array3.length + 1);
            array3[count] = it.next();
            System.out.print(array3[count] + " ");
        }
        System.out.println();
    }

    public static void OneArray() throws IOException {
        int[] array1 = CreateArray();
        int[] array2 = CreateArray();
        int size3 = array1.length > array2.length ? array2.length : array1.length;
        int count = 0;
        int[] array3 = new int[size3];
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] != array2[j]) {
                    count++;
                }
            }
            if (count == array2.length) {

                array3[array3.length - 1] = array1[i];
                array3 = Arrays.copyOf(array3, array3.length + 1);
                System.out.print(array1[i] + " ");
            }
            count = 0;
        }
        System.out.println();
    }

    public static void Delete() throws IOException {
        Set<Integer> set = new HashSet<Integer>();
        int[] array = CreateArray();
        int[] array3 = new int[0];
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] != array[j]) {
                    count++;
                }
            }
            if (count == array.length - 1) {
                set.add(array[i]);
            }
            count = 0;
        }

        System.out.println();
        Iterator<Integer> it = set.iterator();
        int i = 0;
        while (it.hasNext()) {
            array3 = Arrays.copyOf(array3, array3.length + 1);
            array3[i] = it.next();
            System.out.print(array3[i] + " ");
        }
    }

}

