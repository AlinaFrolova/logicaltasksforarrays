package com.program;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.program.LogicalTasksForArrays.AddUniqueValues;
import static com.program.LogicalTasksForArrays.Delete;
import static com.program.LogicalTasksForArrays.OneArray;
import static com.program.LogicalTasksForArrays.commonArray;

public class Menu {
    Doors doors = new Doors();
    public void choose() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Press 1 to form the array that consists of the " +
                "common elements: ");
        System.out.println("Press 2 to form the array that consists of the" +
                " elements which are only in the first array: ");
        System.out.println("Press 3 to delete the elements of the array that " +
                "repeat more than two times: ");
        System.out.println("Press 4 to create the array without numbers that repeat: ");
        System.out.println("Press 5 to play the game: ");
        int i = Integer.parseInt(reader.readLine());
        switch (i){
            case 1:
                commonArray();
                choose();
            case 2:
                OneArray();
                choose();
            case 3:
                Delete();
                choose();
            case 4:
                AddUniqueValues();
                choose();
            case 5:
                System.out.print("Numbers of doors: ");
                doors.Doors();
                doors.OpenDoor();
        }

    }
}
